from bs4 import BeautifulSoup
import urllib.request

def get_html(url):
    response = urllib.request.urlopen(url)
    return response.read()

def parse(html):
    soup = BeautifulSoup(html,"html.parser")
    ide = soup.find('dd',itemprop = 'applicationNumber').text
    title = soup.find('span',itemprop = 'title').text
    title = title.strip()
    classe = soup.find('meta',itemprop = 'FirstCode').parent
    classe = classe.find('span',itemprop = 'Code').text
    area = classe[0:4]
    aulis = []
    hlp = soup.find_all('dd',itemprop = 'inventor')
    for au in hlp:
        aulis.append(au.text)
    author = ', '.join(aulis)
    country = soup.find('dd',itemprop = 'countryName').text
    try:
        lang = soup.find('dd',itemprop = 'otherLanguages').find('span',itemprop = 'name').text
    except:
        lang = 'English'
    abstract = soup.find('div',class_ = 'abstract').text
    datefil = soup.find('time',itemprop = 'filingDate').text
    datepub = soup.find('time',itemprop = 'publicationDate').text
    link = soup.find('link',rel = 'canonical').get('href')
    lis = [ide, classe, area, title, author, country, lang, abstract, datefil, datepub, link]
    row = '; '.join(lis)
    ##print(row)
    return(row)
    

def main():
    ##f = open('links.txt', 'r', encoding = 'utf-8')
    t = open('information.csv', 'a', encoding = 'utf-8')
    ##for line in f:
    t.write(parse(get_html('https://patents.google.com/patent/FR2787217A1/en?q=((mainframe))&country=FR&oq=((mainframe))+country:FR')))
    t.write('\n')
    ##f.close()
    t.close()
        
if __name__ == '__main__':
    main()
